
#include "MPU6050.h"
#include "stm32f4xx_hal.h"

#define WRITE 0
#define READ  1

static uint8_t MPU6050_resetBits(uint8_t regAddr, uint8_t val);
static uint8_t MPU6050_setBits(uint8_t regAddr, uint8_t val);
static uint8_t MPU6050_I2C_ByteRead(uint8_t regaddr);
static void MPU6050_I2C_ByteWrite(uint8_t writeAddr, uint8_t data);
uint8_t MPU6050_GetFullScaleAccelRange();
void MPU6050_SetFullScaleAccelRange(uint8_t range);
void MPU6050_SetClockSource(uint8_t source);
uint8_t MPU6050_GetDeviceID();
uint8_t MPU6050_GetIntStatus(void);
uint8_t MPU6050_TestConnection();
void MPU6050_StartCycleMode(uint8_t wakeCtrl);



extern I2C_HandleTypeDef hi2c1;



void MPU6050_StartCycleMode(uint8_t wakeCtrl) 
{
	mpu6050.setBits(MPU6050_RA_PWR_MGMT_2, (wakeCtrl & 0x03) << 6);
	mpu6050.setBits(MPU6050_RA_PWR_MGMT_1, 1 << 5);	
}



/** Verify the I2C connection.
 * Make sure the device is connected and responds as expected.
 * @return True if connection is valid, FALSE otherwise
 */
static uint8_t MPU6050_TestConnection() 
{
	if(0x68 == MPU6050_GetDeviceID()) //0b110100; 8-bit representation in hex = 0x34
		return TRUE;
	else
		return FALSE;
}

static uint8_t MPU6050_GetIntStatus(void) {
	uint8_t tmp = 0;
	tmp = mpu6050.readByte(MPU6050_RA_INT_STATUS);
	return tmp;
}


/** Get Device ID.
 * This register is used to verify the identity of the device (0b110100).
 * @return Device ID (should be 0x68, 104 dec, 150 oct)
 * @see MPU6050_RA_WHO_AM_I
 * @see MPU6050_WHO_AM_I_BIT
 * @see MPU6050_WHO_AM_I_LENGTH
 */
static uint8_t MPU6050_GetDeviceID()
{
	uint8_t itmp;
	itmp = mpu6050.readByte(MPU6050_RA_WHO_AM_I);
	return itmp; 
}
/** Set clock source setting.
 * An internal 8MHz oscillator, gyroscope based clock, or external sources can
 * be selected as the MPU-60X0 clock source. When the internal 8 MHz oscillator
 * or an external source is chosen as the clock source, the MPU-60X0 can operate
 * in low power modes with the gyroscopes disabled.
 *
 * Upon power up, the MPU-60X0 clock source defaults to the internal oscillator.
 * However, it is highly recommended that the device be configured to use one of
 * the gyroscopes (or an external clock source) as the clock reference for
 * improved stability. The clock source can be selected according to the following table:
 *
 * <pre>
 * CLK_SEL | Clock Source
 * --------+--------------------------------------
 * 0       | Internal oscillator
 * 1       | PLL with X Gyro reference
 * 2       | PLL with Y Gyro reference
 * 3       | PLL with Z Gyro reference
 * 4       | PLL with external 32.768kHz reference
 * 5       | PLL with external 19.2MHz reference
 * 6       | Reserved
 * 7       | Stops the clock and keeps the timing generator in reset
 * </pre>
 *
 * @param source New clock source setting
 * @see MPU6050_GetClockSource()
 * @see MPU6050_RA_PWR_MGMT_1
 * @see MPU6050_PWR1_CLKSEL_BIT
 * @see MPU6050_PWR1_CLKSEL_LENGTH
 */
static void MPU6050_SetClockSource(uint8_t source) 
{
	mpu6050.writeByte(MPU6050_RA_PWR_MGMT_1, source & 0x07);
}





/** Get full-scale accelerometer range.
 * The FS_SEL parameter allows setting the full-scale range of the accelerometer
 * sensors, as described in the table below.
 *
 * <pre>
 * 0 = +/- 2g
 * 1 = +/- 4g
 * 2 = +/- 8g
 * 3 = +/- 16g
 * </pre>
 *
 * @return Current full-scale accelerometer range setting
 * @see MPU6050_ACCEL_FS_2
 * @see MPU6050_RA_ACCEL_CONFIG
 * @see MPU6050_ACONFIG_AFS_SEL_BIT
 * @see MPU6050_ACONFIG_AFS_SEL_LENGTH
 */
static uint8_t MPU6050_GetFullScaleAccelRange() 
{
	uint8_t itmp;
	itmp = mpu6050.readByte(MPU6050_RA_ACCEL_CONFIG) >> 3;
	return itmp;
}

/** Set full-scale accelerometer range.
 * @param range New full-scale accelerometer range setting
 * @see MPU6050_GetFullScaleAccelRange()
 */
static void MPU6050_SetFullScaleAccelRange(uint8_t range) 
{
	mpu6050.setBits(MPU6050_RA_ACCEL_CONFIG, ((range & 0x03)<<3));
}






static void MPU6050_SoftReset()
{
	MPU6050_setBits(MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_DEVICE_RESET_BIT);
	while(MPU6050_PWR1_DEVICE_RESET_BIT == MPU6050_I2C_ByteRead(MPU6050_RA_PWR_MGMT_1));
}

/**
* @brief  Initializes the I2C peripheral used to drive the MPU6050
* @param  None
* @return None
*/
static void MPU6050_I2C_Init()
{
//    GPIO_Init(MPU6050_SDA_GPIO, MPU6050_SDA_PIN, GPIO_Mode_Out_OD_HiZ_Fast); 
//    GPIO_Init(MPU6050_SCL_GPIO, MPU6050_SCL_PIN, GPIO_Mode_Out_OD_HiZ_Fast);
//    
//    CLK_PeripheralClockConfig(MPU6050_CLK_Peripheral_I2C1,ENABLE);
//    I2C_Init(MPU6050_I2C, MPU6050_Speed, 12, I2C_Mode_I2C, 
//                    I2C_DutyCycle_2, I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);
//    I2C_Cmd(MPU6050_I2C, ENABLE); 

}
static uint8_t MPU6050_setBits(uint8_t regAddr, uint8_t val)
{
  	uint8_t itmp;
	itmp = MPU6050_I2C_ByteRead(regAddr);
	itmp = itmp | val;
	MPU6050_I2C_ByteWrite(regAddr, itmp);
	
	return MPU6050_I2C_ByteRead(regAddr);	
}

static uint8_t MPU6050_resetBits(uint8_t regAddr, uint8_t val)
{
  	uint8_t itmp;
	itmp = MPU6050_I2C_ByteRead(regAddr);	
	itmp = itmp & ~val;
	MPU6050_I2C_ByteWrite(regAddr, itmp);
	
	return MPU6050_I2C_ByteRead(regAddr);	
}



static void MPU6050_I2C_ByteWrite(uint8_t writeAddr, uint8_t data)
{
//    WAIT_UNTIL_BUSY;
//    I2C_START
//    {
//        /* Test on EV5 and clear it */
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_MODE_SELECT));
//        /* Send MPU6050 address for write */
//        I2C_Send7bitAddress(MPU6050_I2C, MPU6050_DEFAULT_ADDRESS + WRITE, I2C_Direction_Transmitter);
//        /* Test on EV6 and clear it */
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
//        /* Send the MPU6050's internal address to write to */
//        I2C_SendData(MPU6050_I2C, writeAddr);
//        /* Test on EV8 and clear it */
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
//        
//        /* Send the byte to be written */
//        I2C_SendData(MPU6050_I2C, data);
//        /* Test on EV8 and clear it */
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
//    }
//    I2C_STOP;
}



static uint8_t MPU6050_I2C_ByteRead(uint8_t regaddr)
{
    volatile uint8_t tmp1, tmp2;
//    
//    WAIT_UNTIL_BUSY;
//    
//    I2C_AcknowledgeConfig(MPU6050_I2C, ENABLE); // Enable I2C acknowledgment
//    I2C_START
//    {
//        while (!I2C_CheckEvent(MPU6050_I2C,I2C_EVENT_MASTER_MODE_SELECT));
//
//        I2C_Send7bitAddress(MPU6050_I2C, MPU6050_DEFAULT_ADDRESS + WRITE, I2C_Direction_Transmitter); // Device address and direction
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
//
//        I2C_SendData(MPU6050_I2C, regaddr); // Mode register
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
//
//        I2C_GenerateSTART(MPU6050_I2C, ENABLE);
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_MODE_SELECT));
//
//        I2C_Send7bitAddress(MPU6050_I2C, MPU6050_DEFAULT_ADDRESS + READ, I2C_Direction_Receiver);  // Device address and direction
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
//
//		
//        while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED));
//        tmp1 = I2C_ReceiveData(MPU6050_I2C); // Reading data from the buffer
//        I2C_AcknowledgeConfig(MPU6050_I2C, DISABLE); // Disable I2C acknowledgment
//
//    }
//    I2C_STOP;
//    
//    while (!I2C_CheckEvent(MPU6050_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED));
//    tmp2 = I2C_ReceiveData(MPU6050_I2C); // Reading data from the buffer

    return tmp1;
}


void mpu_write_byte(uint8_t addr, uint8_t value)
{
    HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDRESS_AD0_LOW<<1, 
                      addr, I2C_MEMADD_SIZE_8BIT, &value, 1, 1000);
}

uint8_t mpu_read_byte(uint8_t addr)
{
    uint8_t tmp, value;
    HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDRESS_AD0_LOW<<1, 
                                   addr, 
                                   I2C_MEMADD_SIZE_8BIT, 
                                   &tmp, 
                                   1, 
                                   1000);
    value = tmp;
    return value ;
}

void mpu_read_pkt(uint8_t addr, uint8_t* ptr, uint16_t size)
{
    HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDRESS_AD0_LOW<<1, 
                                   addr, 
                                   I2C_MEMADD_SIZE_8BIT, 
                                   ptr, 
                                   size, 
                                   1000);
}

int16_t combine(uint8_t upper, uint8_t lower){
	int16_t result = 0;
	result = ((upper << 8) | lower);
	return result & 0xFFFF;
}

/**
  * @brief  Структура дравера Mpu6050.
*/
const struct Mpu6050_driver mpu6050 = 
{
	.init_i2c   	= MPU6050_I2C_Init,
	.softReset   	= MPU6050_SoftReset,
	.writeByte		= MPU6050_I2C_ByteWrite,
	.readByte 		= MPU6050_I2C_ByteRead,
	.setBits 		= MPU6050_setBits,
	.resetBits 		= MPU6050_resetBits,
	.getFullScaleAccelRange	= MPU6050_GetFullScaleAccelRange,
	.setFullScaleAccelRange	= MPU6050_SetFullScaleAccelRange,
	.setClockSource	= MPU6050_SetClockSource,
	.getDeviceID 	= MPU6050_GetDeviceID,
	.getIntStatus   = MPU6050_GetIntStatus,
	.testConnection = MPU6050_TestConnection,
//	.startCycleMode = MPU6050_StartCycleMode,
};










/**
 * @}
 */ /* end of group MPU6050_Library */
