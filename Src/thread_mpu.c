/* Includes ------------------------------------------------------------------*/
//#include "utils.h"
#include "MPU6050.h" 

/* Private functions ---------------------------------------------------------*/
/*
void getGyro(s16* ix, s16* iy, s16* iz);
uint16_t GetVcc(void);
s16 x, y, z;
s16 x_offset, y_offset, z_offset;


extern u8 Y[34];
extern u8 Z[34];
u16 tmpVCC;

PT_THREAD (thread_radioSend(pt_t *pt));
pt_t radioSend_pt;

INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
{
	EXTI->SR1 = EXTI_MPU_PIN;
	messages[MPU_ISR_MSG] = true;
}


PT_THREAD (thread_mpu(pt_t *pt))
{
  	PT_BEGIN(pt);
	
	static u8 lower, upper;
	static u8 data_ready = 0;
	static u8 cnt = 30;
	x = y = z = 0;
	
	disableInterrupts();
	do
	{
	  	GPIO_Init(MPU6050_PWR_GPIO, MPU6050_PWR_PIN, GPIO_Mode_In_FL_No_IT);
		enableInterrupts();
			PT_DELAY_MS(MPU_TIMER, 50);
		disableInterrupts();
		GPIO_Init(MPU6050_PWR_GPIO, MPU6050_PWR_PIN, GPIO_Mode_Out_PP_High_Slow);
		enableInterrupts();
			PT_DELAY_MS(MPU_TIMER, 9);
		disableInterrupts();
		
		I2C_SoftwareResetCmd(I2C1, ENABLE);
		I2C_SoftwareResetCmd(I2C1, DISABLE);
		mpu6050.init_i2c();
		
		if(FALSE == mpu6050.testConnection) 
		{
			continue;
		}
		mpu6050.softReset();
		
		enableInterrupts();
			PT_DELAY_MS(MPU_TIMER, 9);
		disableInterrupts();
		
		mpu6050.setBits(MPU6050_RA_GYRO_CONFIG, (MPU6050_GYRO_FS_2000 & 0x03) << 3);
		mpu6050.writeByte(MPU6050_RA_INT_ENABLE, MPU6050_INTERRUPT_DATA_RDY_BIT);
		mpu6050.writeByte(MPU6050_RA_PWR_MGMT_1, 0x08 | 0x01);     // ���������� ������. �������, ������������ �� X ��� ���������
		mpu6050.writeByte(MPU6050_RA_PWR_MGMT_2, (7 << 3)); // ���������� ��� �������������
		mpu6050.writeByte(MPU6050_RA_SMPLRT_DIV, 0x00); // �������� ������� ������� 
		mpu6050.writeByte(MPU6050_RA_INT_PIN_CFG, (1 << 7)|(1 << 4)); // �������� ����, ������� ���� ��� ����� ������
		mpu6050.writeByte(MPU6050_RA_CONFIG, 0x00); // 
		//Check that the MPU has been awoken
		if((0x08 | 0x01) != mpu6050.readByte(MPU6050_RA_PWR_MGMT_1))
		{
			GPIO_Init(MPU6050_PWR_GPIO, MPU6050_PWR_PIN, GPIO_Mode_In_FL_No_IT);
			continue;
		}

		enableInterrupts();
			PT_DELAY_MS(MPU_TIMER, 50);
		disableInterrupts();

		
		//Wait until the MPU has data ready
		data_ready = mpu6050.getIntStatus();
		
	} while(MPU6050_INTERRUPT_DATA_RDY_BIT != (data_ready & MPU6050_INTERRUPT_DATA_RDY_BIT));
	data_ready = 0;
	
	lower = mpu6050.readByte(MPU6050_RA_GYRO_XOUT_L);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_XOUT_H);
	x_offset = (((s16)upper << 8) | ((s16)lower << 0));
	
	lower = mpu6050.readByte(MPU6050_RA_GYRO_YOUT_L);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_YOUT_H);
	y_offset = (((s16)upper << 8) | ((s16)lower << 0));
	
	lower = mpu6050.readByte(MPU6050_RA_GYRO_ZOUT_L);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_ZOUT_H);
	z_offset = (((s16)upper << 8) | ((s16)lower << 0));
	
	
	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE); 
	ADC_Init(ADC1, ADC_ConversionMode_Single, ADC_Resolution_12Bit,ADC_Prescaler_2);
	ADC_VrefintCmd(ENABLE);
	ADC_Cmd(ADC1, ENABLE);
	
	tmpVCC = GetVcc();
	ADC_DeInit(ADC1);
	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, DISABLE); 
	


	cnt = 10;
	enableInterrupts();
	do
	{
	
		disableInterrupts();
		data_ready = mpu6050.getIntStatus();
		if(MPU6050_INTERRUPT_DATA_RDY_BIT == (data_ready & MPU6050_INTERRUPT_DATA_RDY_BIT))
		{
			data_ready = 0;
			
			
			getGyro(&x, &y, &z);							
			tim.reset(MPU_TIMER);
			thread_radioSend(&radioSend_pt);
			tim.reset(MPU_TIMER);
		}
		enableInterrupts();
		
		PT_DELAY_MS(RADIO_TIMER, 50);	
	
	} while(--cnt > 0);

	
	
	disableInterrupts();
	
	GPIO_Init(MPU6050_PWR_GPIO, MPU6050_PWR_PIN, GPIO_Mode_Out_OD_HiZ_Fast);

	EXTI->SR1 = EXTI_GDO0_PIN;
	for (u8 i = 0; i < sizeof(Y); Y[i++] = 0);
	x = y = z = 0;
	enableInterrupts();
	
	
	PT_END(pt);
}*/


